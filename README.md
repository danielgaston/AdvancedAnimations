# AdvancedAnimations
Advanced Animations with UIKit (Objective-C).<br/>
iOS Test application to learn to combine and coordinate between multiple animations.


The app references to WWDC2017 ["Advanced Animations with UIKit"](https://developer.apple.com/videos/play/wwdc2017/230/) Session.<br/>
Ported to ObjC from [this](https://github.com/kane-liu/AdvancedAnimations) repo.

![preview](https://gitlab.com/danielgaston/AdvancedAnimations/raw/master/AdvancedAnimations/AdvancedAnimations/gif/advancedAnimationsReduced.gif)

- Features
- [x] Using UIViewPropertyAnimator
- [x] View frame animator
- [x] View blur animator
- [x] View scale animator
- [x] View corner radius animator
- [x] View key frame animator
- [x] Be able to interrute 
- [x] Tap & pan gesture

- Issues
- [ ] UIViewPropertyAnimator only supported from iOS10
- [ ] Some features only work on iOS11 like corner radius "maskedCorners"
- [ ] When animators are interruted blur effect not works well


